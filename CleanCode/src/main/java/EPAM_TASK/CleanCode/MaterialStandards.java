package EPAM_TASK.CleanCode;

public interface MaterialStandards {
    Integer standardMaterials();
    Integer aboveStandardMaterials();
    Integer highStandardMaterials();
}
